<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class List_berita extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['berita_get']['limit'] = 500; // 500 requests per hour per user/key
  
    }

    public function berita_get()
    {

        // If the id parameter doesn't exist return all the users
        
        
        $cari = "";
        if(empty($_GET['cari'])){
            $cari="apa";
        }else{
            $cari=$_GET['cari'];
        }


        $url = "https://newsapi.org/v2/everything?q=".$cari."&apiKey=d980dd70569244f589f7536875556d81";
    
        $json = file_get_contents($url);
        $obj = json_decode($json);

        
        
    
            // Check if the users data store contains users (in case the database result returns NULL)
            if ($obj)
            {
                // Set the response and exit
                $this->response($obj, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
            }
            else
            {
                // Set the response and exit
                $this->response([
                    'status' => FALSE,
                    'message' => 'No news were found'
                ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
            }
        

      
    }

    
  

}
